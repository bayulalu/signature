@extends('layout.master')
@section('title', 'Upload Document')

@section('contain')
<br>
<h1 class="text-center"> Print Document </h1><br>
<form method="post" action="{{ route('doc2') }}" enctype="multipart/form-data">
    <div class="row g-3 align-items-center">
      
        <div class="col-auto">
            <label for="" class="col-form-label">type Signature</label>
        </div>

        

        <div class="col-auto">
            <select name="signature" id="" class="form-control">
                @foreach ($datas as $item)
                <option value="{{$item->id}}">{{$item->description}}</option>

                @endforeach

            </select>
        </div>

        <div class="col-auto">
            <button type="submit" class="btn btn-primary">print document</button>

        </div>
    </div>
    @csrf

</form>

@endsection