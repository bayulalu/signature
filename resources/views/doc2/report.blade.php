<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex">
    <title>Doc</title>
    {{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous"> --}}

</head>
<style>
    .text-center{
        text-align: center;
    }
</style>
<body>
    <div class="container">
        <br><br>
        <h1 class="text-center">Document 2</h1>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nesciunt neque provident magnam maxime tempora perferendis velit iste ducimus quod, quia dicta ratione ipsa error totam voluptatum dolor officiis qui id.</p>
        <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Expedita sit quisquam ea saepe id enim commodi unde harum dolore sint natus corrupti voluptatem excepturi earum, cum maiores? Quasi, provident numquam.Provident earum consectetur perspiciatis officia repudiandae, molestias distinctio sit quod excepturi fuga, non suscipit eveniet accusamus magni error porro atque corporis ratione officiis necessitatibus debitis? In nesciunt animi alias officia!</p>
        <br><br>
        <div style="float: right;">
            <table>
                <tbody>
                <tr>
                    <td style="width: 400px !important;">
                     
                    </td>
                    <td style="width: 100px">&nbsp;</td>
                    <td style="width: 300px">
                        Signature,  
                        <br>
                        <img height="100" src="{{ asset('asset/'.$img->img) }}" alt="">
                        <br>
                        {{$img->description}}
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    
    
    </div>
 
</body>
</html>
