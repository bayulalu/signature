@extends('layout.master')
@section('title', 'Upload Signature')

@section('contain')
<br>
        <br>
        <h1 class="text-center"> Upload Signature </h1>
        <br>
        <form method="post" action="{{ route('img') }}" enctype="multipart/form-data" >

            <div class="row g-3 align-items-center">
                <div class="col-auto">
                  <label for="inputPassword6" class="col-form-label">File Picture</label>
                </div>
                <div class="col-auto">
                    <input type="file" required name="file" class="form-control">
                </div>
                <div class="col-auto">
                    <input type="text" required name="ket" class="form-control" placeholder="name">

                </div>
              </div>
            @csrf
            <br>
            
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>

          <hr>
          <br>
          <br>
          <table class="table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Signature</th>
                <th scope="col">name</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($datas as $key => $data)
                    <tr>
                        <th scope="row">{{$key + 1}}</th>
                        <td> <img src="{{ asset('asset/'.$data->img) }}" height="120" alt=""> </td>
                        <td>{{$data->description}}</td>
                  </tr>
                @endforeach

             
             
            </tbody>
          </table>
    @endsection
