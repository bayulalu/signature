<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ImgController@index')->name('img');
Route::post('/', 'ImgController@create')->name('img');

Route::get('/doc', 'DocController@index')->name('doc');
Route::post('/doc', 'DocController@create')->name('doc');

Route::get('/doc2', 'DocController@doc')->name('doc2');
Route::post('/doc2', 'DocController@report')->name('doc2');





