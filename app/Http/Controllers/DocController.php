<?php

namespace App\Http\Controllers;

use App\Img;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;

class DocController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = Img::get();
        return view('doc.index', compact('datas'));
    }


    public function tes()
    {

        // Get submitted form data
        $apiKey = $_POST["apiKey"]; // The authentication key (API Key). Get your own by registering at https://app.pdf.co


        // 1. RETRIEVE THE PRESIGNED URL TO UPLOAD THE FILE.
        // * If you already have the direct PDF file link, go to the step 3.

        // Create URL
        $url = "https://api.pdf.co/v1/file/upload/get-presigned-url" .
            "?name=" . urlencode($_FILES["file"]["name"]) .
            "&contenttype=application/octet-stream";

        // Create request
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("x-api-key: " . $apiKey));
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        // Execute request
        $result = curl_exec($curl);

        if (curl_errno($curl) == 0) {
            $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            if ($status_code == 200) {
                $json = json_decode($result, true);

                // Get URL to use for the file upload
                $uploadFileUrl = $json["presignedUrl"];
                // Get URL of uploaded file to use with later API calls
                $uploadedFileUrl = $json["url"];

                // 2. UPLOAD THE FILE TO CLOUD.

                $localFile = $_FILES["file"]["tmp_name"];
                $fileHandle = fopen($localFile, "r");

                curl_setopt($curl, CURLOPT_URL, $uploadFileUrl);
                curl_setopt($curl, CURLOPT_HTTPHEADER, array("content-type: application/octet-stream"));
                curl_setopt($curl, CURLOPT_PUT, true);
                curl_setopt($curl, CURLOPT_INFILE, $fileHandle);
                curl_setopt($curl, CURLOPT_INFILESIZE, filesize($localFile));

                // Execute request
                curl_exec($curl);

                fclose($fileHandle);

                if (curl_errno($curl) == 0) {
                    $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

                    if ($status_code == 200) {
                        // Display uploaded File URL
                        echo "<p>Uploaded File: " . $uploadFileUrl . "</p>";
                    } else {
                        // Display request error
                        echo "<p>Status code: " . $status_code . "</p>";
                        echo "<p>" . $result . "</p>";
                    }
                } else {
                    // Display CURL error
                    echo "Error: " . curl_error($curl);
                }
            } else {
                // Display service reported error
                echo "<p>Status code: " . $status_code . "</p>";
                echo "<p>" . $result . "</p>";
            }

            curl_close($curl);
        } else {
            // Display CURL error
            echo "Error: " . curl_error($curl);
        }
    }

    public function replaceImageFromPdf($uploadedFileUrl, $signature)
    {
        $url = "https://api.pdf.co/v1/pdf/edit/replace-text-with-image";
        $apiKey = env("TOKEN_PDF");

        // Prepare requests params
        $parameters = array();
        $parameters["name"] = "result.pdf";
        $parameters["url"] = $uploadedFileUrl;
        $parameters["searchString"] = "signature";

        // You can also upload your own file into PDF.co and use it as url. Check "Upload File" samples for code snippets: https://github.com/bytescout/pdf-co-api-samples/tree/master/File%20Upload/    
        $parameters["replaceImage"] = asset('public/asset/' . $signature);

        // Create Json payload
        $data = json_encode($parameters);

        // Create request
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("x-api-key: " . $apiKey, "Content-type: application/json"));
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

        // Execute request
        $result = curl_exec($curl);

        if (curl_errno($curl) == 0) {
            $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            if ($status_code == 200) {
                $json = json_decode($result, true);

                if (!isset($json["error"]) || $json["error"] == false) {
                    $resultFileUrl = $json["url"];

                    // Display link to the result file
                    echo "<div><h2>Conversion Result:</h2><a href='" . $resultFileUrl . "' target='_blank'>" . $resultFileUrl . "</a></div>";
                } else {
                    // Display service reported error
                    echo "<p>Error: " . $json["message"] . "</p>";
                }
            } else {
                // Display request error
                echo "<p>Status code: " . $status_code . "</p>";
                echo "<p>" . $result . "</p>";
            }
        } else {
            // Display CURL error
            echo "Error: " . curl_error($curl);
        }

        // Cleanup
        curl_close($curl);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $apiKey ='bayulalu45@gmail.com_c93acad389e9089e2431d9317c83d8672b953339101a8e76608dafe5a9ef0c6e33dbdea0'; // The authentication key (API Key). Get your own by registering at https://app.pdf.co

        // Create URL
        $url = "https://api.pdf.co/v1/file/upload/get-presigned-url" .
            "?name=" . urlencode($request->file('file')) .
            "&contenttype=application/octet-stream";

        // Create request
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("x-api-key: " . $apiKey));
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        // Execute request
        $result = curl_exec($curl);

        if (curl_errno($curl) == 0) {
            $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            // dd($status_code);
            if ($status_code == 200) {
                $json = json_decode($result, true);

                // Get URL to use for the file upload
                $uploadFileUrl = $json["presignedUrl"];
                // Get URL of uploaded file to use with later API calls
                $uploadedFileUrl = $json["url"];

                // 2. UPLOAD THE FILE TO CLOUD.

                $localFile = $request->file('file');
                $fileHandle = fopen($localFile, "r");

                curl_setopt($curl, CURLOPT_URL, $uploadFileUrl);
                curl_setopt($curl, CURLOPT_HTTPHEADER, array("content-type: application/octet-stream"));
                curl_setopt($curl, CURLOPT_PUT, true);
                curl_setopt($curl, CURLOPT_INFILE, $fileHandle);
                curl_setopt($curl, CURLOPT_INFILESIZE, filesize($localFile));

                // Execute request
                curl_exec($curl);

                fclose($fileHandle);

                if (curl_errno($curl) == 0) {
                    $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

                    if ($status_code == 200) {
                        // Display uploaded File URL
                        echo "<p>Uploaded File: " . $uploadFileUrl . "</p>";
                    } else {
                        // Display request error
                        // echo "<p>Status code: " . $status_code . "</p>";
                        // echo "<p>" . $result . "</p>";
                    }
                } else {
                    // Display CURL error
                    echo "Error: " . curl_error($curl);
                }
            } else {
                // Display service reported error
                echo "<p>Status code: " . $status_code . "</p>";
                echo "<p>" . $result . "</p>";
            }

            curl_close($curl);
        } else {
            // Display CURL error
            // echo "Error: " . curl_error($curl);
        }


        // 
        // $this->replaceImageFromPdf($request->file('file'), $request->signature);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function doc(){
        $datas = Img::get();
        return view('doc2.index', compact('datas'));
    }

    public function report(Request $request){
        $img = Img::where('id', $request->signature)->first();

        $pdf = PDF::loadview('doc2.report', compact('img'));
        return $pdf->setPaper('A5', 'landscape')->stream('Doc-'.$img->description. '.pdf');

        // return view('doc2.report', compact('img'));
    }
}
